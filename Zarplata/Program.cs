﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zarplata
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("Задача: Зарплата Сотрудников");

            int Petya, Sasha, Vitya, result;
            result = 0;

            try
            {

                Console.Write("Введите зарплату Пети: ");
                Petya = int.Parse(Console.ReadLine());

                Console.Write("Введите зарплату Саши: ");
                Sasha = int.Parse(Console.ReadLine());

                Console.Write("Введите зарплату Вити: ");
                Vitya = int.Parse(Console.ReadLine());

                // зарплаты меньше 10 в 5 степени.
                if ((Petya >= Math.Pow(10, 5))
                    || (Sasha >= Math.Pow(10, 5))
                   || (Vitya >= Math.Pow(10, 5)))
                {
                    Console.WriteLine("Неверные данные. Программа будет закрыта. ");
                    Console.ReadKey();
                    return;

                }

                /// условие 1
                if ((Petya <= Sasha) && (Petya <= Vitya) && (Sasha <= Vitya))
                {
                    result = Vitya - Petya;
                }
                else if ((Petya <= Sasha) && (Petya <= Vitya) && (Vitya <= Sasha))
                {
                    result = Sasha - Petya;
                }

                /// условие 2
                if ((Sasha <= Petya) && (Sasha <= Vitya) && (Petya <= Vitya))
                {
                    result = Vitya - Sasha;
                }
                else if ((Sasha <= Petya) && (Sasha <= Vitya) && (Vitya <= Petya))
                {
                    result = Petya - Sasha;
                }

                /// условие 3
                if ((Vitya <= Petya) && (Vitya <= Sasha) && (Sasha <= Petya))
                {
                    result = Petya - Vitya;
                }
                else if ((Vitya <= Petya) && (Vitya <= Sasha) && (Petya <= Sasha))
                {
                    result = Sasha - Vitya;
                }
                Console.WriteLine($"Результат: {result}");
            }
            catch
            {
                Console.WriteLine("Неверные данные. Программа будет закрыта. ");
            }

            Console.ReadKey();

        }
    }
}
